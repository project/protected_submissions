<?php

namespace Drupal\protected_submissions\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * ProtectedSubmissionsForm class.
 */
class ProtectedSubmissionsForm extends ConfigFormBase {

  /**
   * Protected $moduleHandler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /**
   * ProtectedSubmissionsForm constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The moduler handler.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ModuleHandlerInterface $moduleHandler) {
    parent::__construct($config_factory);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'protected_submissions_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    // Form constructor.
    $form = parent::buildForm($form, $form_state);
    // Default settings.
    $config = $this->config('protected_submissions.settings');

    $language_scripts = [
      'Adlam' => 'Adlam',
      'Ahom' => 'Ahom',
      'Anatolian Hieroglyphs' => 'Anatolian Hieroglyphs',
      'Arabic' => 'Arabic',
      'Aramaic' => 'Aramaic',
      'Armenian' => 'Armenian',
      'Avestan' => 'Avestan',
      'Balinese' => 'Balinese',
      'Bamum' => 'Bamum',
      'Bassa Vah' => 'Bassa Vah',
      'Batak' => 'Batak',
      'Bengali and Assamese' => 'Bengali and Assamese',
      'Bhaiksuki' => 'Bhaiksuki',
      'Bopomofo' => 'Bopomofo',
      'Brahmi' => 'Brahmi',
      'Buginese' => 'Buginese',
      'Buhid' => 'Buhid',
      'Canadian Aboriginal' => 'Canadian Aboriginal',
      'Carian' => 'Carian',
      'Caucasian Albanian' => 'Caucasian Albanian',
      'Chakma' => 'Chakma',
      'Cham' => 'Cham',
      'Cherokee' => 'Cherokee',
      'Chorasmian' => 'Chorasmian',
      'CJK' => 'CJK',
      'Coptic' => 'Coptic',
      'Cuneiform' => 'Cuneiform',
      'Currency Symbols' => 'Currency Symbols',
      'Cypriot Syllabary' => 'Cypriot Syllabary',
      'Cyrillic' => 'Cyrillic',
      'Deseret' => 'Deseret',
      'Devanagari' => 'Devanagari',
      'Diacritical Marks' => 'Diacritical Marks',
      'Dives Akuru' => 'Dives Akuru',
      'Dogra' => 'Dogra',
      'Egyptian Hieroglyphs' => 'Egyptian Hieroglyphs',
      'Elbasan' => 'Elbasan',
      'Elymaic' => 'Elymaic',
      'Ethiopic' => 'Ethiopic',
      'Georgian' => 'Georgian',
      'Glagolitic' => 'Glagolitic',
      'Gothic' => 'Gothic',
      'Grantha' => 'Grantha',
      'Greek' => 'Greek',
      'Gujarati' => 'Gujarati',
      'Gunjala Gondi' => 'Gunjala Gondi',
      'Gurmukhi' => 'Gurmukhi',
      'Hangul Jamo' => 'Hangul Jamo',
      'Hangul Syllables' => 'Hangul Syllables',
      'Hanifi Rohingya' => 'Hanifi Rohingya',
      'Hanunoo' => 'Hanunoo',
      'Hatran' => 'Hatran',
      'Hebrew' => 'Hebrew',
      'Hiragana' => 'Hiragana',
      'Javanese' => 'Javanese',
      'Kaithi' => 'Kaithi',
      'Kana' => 'Kana',
      'Kanbun' => 'Kanbun',
      'Kannada' => 'Kannada',
      'Katakana' => 'Katakana',
      'Kayah Li' => 'Kayah Li',
      'Kharoshthi' => 'Kharoshthi',
      'Khitan Small Script' => 'Khitan Small Script',
      'Khmer' => 'Khmer',
      'Khojki' => 'Khojki',
      'Khudawadi' => 'Khudawadi',
      'Lao' => 'Lao',
      'Latin' => 'Latin',
      'Lepcha' => 'Lepcha',
      'Limbu' => 'Limbu',
      'Lisu' => 'Lisu',
      'Lycian' => 'Lycian',
      'Lydian' => 'Lydian',
      'Mahajani' => 'Mahajani',
      'Makasar' => 'Makasar',
      'Malayalam' => 'Malayalam',
      'Mandaic' => 'Mandaic',
      'Manichaean' => 'Manichaean',
      'Marchen' => 'Marchen',
      'Masaram Gondi' => 'Masaram Gondi',
      'Medefaidrin' => 'Medefaidrin',
      'Meetei Mayek' => 'Meetei Mayek',
      'Mende Kikaku' => 'Mende Kikaku',
      'Meroitic' => 'Meroitic',
      'Miao' => 'Miao',
      'Modi' => 'Modi',
      'Modifier Letters' => 'Modifier Letters',
      'Mongolian' => 'Mongolian',
      'Mro' => 'Mro',
      'Myanmar' => 'Myanmar',
      'Multani' => 'Multani',
      'Nabataean' => 'Nabataean',
      'Nandinagari' => 'Nandinagari',
      'New Tai Lue' => 'New Tai Lue',
      'Newa' => 'Newa',
      'Nko' => 'Nko',
      'Nushu' => 'Nushu',
      'Nyiakeng Puachue Hmong' => 'Nyiakeng Puachue Hmong',
      'Ogham' => 'Ogham',
      'Ol Chiki' => 'Ol Chiki',
      'Old Hungarian' => 'Old Hungarian',
      'Old Italic' => 'Old Italic',
      'Old Permic' => 'Old Permic',
      'Old Persian' => 'Old Persian',
      'Old Sogdian' => 'Old Sogdian',
      'Old Turkic' => 'Old Turkic',
      'Oriya' => 'Oriya' ,
      'Osage' => 'Osage',
      'Osmanya' => 'Osmanya',
      'Pahawh Hmong' => 'Pahawh Hmong',
      'Pahlavi' => 'Pahlavi',
      'Palmyrene' => 'Palmyrene',
      'Parthian' => 'Parthian',
      'Pau Cin Hau' => 'Pau Cin Hau',
      'Phags Pa' => 'Phags Pa',
      'Phoenician' => 'Phoenician',
      'Rejang' => 'Rejang',
      'Runic' => 'Runic',
      'Samaritan' => 'Samaritan',
      'Saurashtra' => 'Saurashtra',
      'Sharada' => 'Sharada',
      'Shavian' => 'Shavian',
      'Siddham' => 'Siddham',
      'Sinhala' => 'Sinhala',
      'Sogdian' => 'Sogdian',
      'Sora Sompeng' => 'Sora Sompeng',
      'Soyombo' => 'Soyombo',
      'Sundanese' => 'Sundanese',
      'Superscripts and Subscripts' => 'Superscripts and Subscripts',
      'Syloti Nagri' => 'Syloti Nagri',
      'Syriac' => 'Syriac',
      'Tagalog' => 'Tagalog',
      'Tagbanwa' => 'Tagbanwa',
      'Tai Le' => 'Tai Le',
      'Tai Tham' => 'Tai Tham',
      'Tai Viet' => 'Tai Viet',
      'Takri' => 'Takri',
      'Tamil' => 'Tamil',
      'Tangut' => 'Tangut',
      'Telugu' => 'Telugu',
      'Thaana' => 'Thaana',
      'Thai' => 'Thai',
      'Tibetan' => 'Tibetan',
      'Tifinagh' => 'Tifinagh',
      'Tirhuta' => 'Tirhuta',
      'Ugaritic' => 'Ugaritic',
      'Vai' => 'Vai',
      'Vedic Extensions' => 'Vedic Extensions',
      'Wancho' => 'Wancho',
      'Warang Citi' => 'Warang Citi',
      'Yezidi' => 'Yezidi',
      'Yi' => 'Yi' ,
      'Yijing' => 'Yijing',
      'Zanabazar Square' => 'Zanabazar Square',
    ];

    $message = t('This module has been discontinued in favor of <a href="@forms">Protected Forms</a>. Please switch to <strong>Protected Forms</strong> as <a href="@patterns">explained in #3281497</a>.',
      ['@forms' => 'https://www.drupal.org/project/protected_forms', '@patterns' => 'https://www.drupal.org/project/protected_submissions/issues/3281497']);

    \Drupal::messenger()->addMessage($message, 'error');


    $form['language'] = array(
      '#type' => 'details',
      '#title' => t('Language settings'),
      '#open' => FALSE,
      '#description' => $this->t('Select language scripts allowed for submission.'),
    );

    $form['language']['allowed_scripts'] = [
      '#type' => 'checkboxes',
      '#options' => $language_scripts,
      '#default_value' => $config->get('protected_submissions.allowed_scripts'),
      '#title' => $this->t('Allowed language scripts'),
      '#required' => TRUE,
    ];

    $form['check_quantity'] = [
      '#type' => 'textfield',
      '#default_value' => $config->get('protected_submissions.check_quantity'),
      '#title' => $this->t('Number of characters to validate'),
      '#size' => 5,
      '#required' => TRUE,
      '#description' => $this->t('Spam texts can be constructed using mixed characters from multiple languages.
      Checking each character would cause performance overhead, so we validate only randomly selected characters.
      Indicate how many characters from submitted text should be validated for allowed language scripts.'),
    ];
    // Page title field.
    $form['reject_message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Reject message:'),
      '#default_value' => $config->get('protected_submissions.reject_message'),
      '#required' => TRUE,
      '#description' => $this->t('Enter a message to display when submission of undesired pattern is attempted.'),
    ];
    // Source text field.
    $form['reject_patterns'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Reject patterns:'),
      '#default_value' => $config->get('protected_submissions.reject_patterns'),
      '#required' => TRUE,
      '#description' => $this->t('Enter words or patterns to reject, separating them with commas or new lines.'),
    ];
    // Give users option to enable/disable logging if Database Logging is on.
    if ($this->moduleHandler->moduleExists('dblog')) {
      $form['log_rejected'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Log rejected submissions'),
        '#default_value' => $config->get('protected_submissions.log_rejected'),
        '#required' => FALSE,
        '#description' => $this->t('Checking on this option could potentially flood <a href="@log">the website\'s log</a>, so use this feature only if you need to analyze the rejected submissions.', ['@log' => '/admin/reports/dblog']),
      ];
    }

    $form['#attached']['library'][] = 'protected_submissions/protected_submissions';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (strlen($form_state->getValue('reject_message')) == 0) {
      $form_state->setErrorByName('user', $this->t('You must enter a reject message to be displayed when an undesired pattern is detected on submission.'));
    }
    elseif (strlen($form_state->getValue('reject_patterns')) == 0) {
      $form_state->setErrorByName('user', $this->t('You must enter the pattern(s) to trigger a submission rejection when matched.'));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $config = $this->config('protected_submissions.settings');
    $config->set('protected_submissions.allowed_scripts', $form_state->getValue('allowed_scripts'));
    $config->set('protected_submissions.check_quantity', $form_state->getValue('check_quantity'));
    $config->set('protected_submissions.reject_message', $form_state->getValue('reject_message'));
    $config->set('protected_submissions.reject_patterns', $form_state->getValue('reject_patterns'));
    $config->set('protected_submissions.log_rejected', $form_state->getValue('log_rejected'));
    $config->save();
    return parent::submitForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'protected_submissions.settings',
    ];
  }

}
