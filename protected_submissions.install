<?php

/**
 * @file
 * Installation functions for protected_submissions module.
 */

use Drupal\user\RoleInterface;

/**
 * Implements hook_install().
 */
function protected_submissions_install() {
  user_role_change_permissions(RoleInterface::AUTHENTICATED_ID, [
    'bypass protected submissions validation' => TRUE,
  ]);
}

/**
 * Implements hook_requirements().
 */
function protected_submissions_requirements($phase) {
  // Provide stats for rejected submissions on the status page.
  $requirements = [];
  $rejected = \Drupal::state()->get('protected_submissions.rejected');
  $requirements['protected_submissions'] = [
    'title' => t('Protected submissions'),
    'value' => t('<span style="color: red">This module has been discontinued. Please switch to <strong>Protected Forms</strong> as <a href="@patterns">explained in #3281497</a>.</span>',
      ['@count' => $rejected, '@patterns' => 'https://www.drupal.org/project/protected_submissions/issues/3281497']),
    'severity' => REQUIREMENT_INFO,
  ];
  return $requirements;
}

/**
 * Implements hook_update_N().
 */
function protected_submissions_update_8001() {
  if (\Drupal::config('protected_submissions.settings')->get('protected_submissions.allowed_scripts') === NULL) {
    \Drupal::configFactory()->getEditable('protected_submissions.settings')
      ->set('protected_submissions.allowed_scripts', ['Latin' => 'Latin'])
      ->save();
  }
  if (\Drupal::config('protected_submissions.settings')->get('protected_submissions.check_quantity') === NULL) {
    \Drupal::configFactory()->getEditable('protected_submissions.settings')
      ->set('protected_submissions.check_quantity', '50')
      ->save();
  }
}

/**
 * Swap rejected submissions from configuration item to state api value.
 */
function protected_submissions_update_8002() {
  $rejected = \Drupal::config('protected_submissions.settings')->get('protected_submissions.rejected');
  if ($rejected) {
    \Drupal::state()->set('protected_submissions.rejected', $rejected);
    \Drupal::configFactory()->getEditable('protected_submissions.settings')
      ->clear('protected_submissions.rejected')
      ->save();
  }
}
