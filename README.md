INTRODUCTION
------------
**Protected Submissions** is a light-weight, non-intrusive spam protection
module that enables rejection of node, comment, webform, user profile, contact
form and revision log submissions which contain undesired language characters
or preset patterns.

HOW IT WORKS
------------
If a user attempts to add a content with a trigger pattern in the name, subject,
body or any other _textarea_  or _textfield_ type field, then the submission
is rejected giving the preset error message.

Admins can configure any role to bypass Protected Submissions validation.

The stats for rejected submissions can be tracked on the Status Report page.

The rejected submissions can be viewed on the Recent log messages page.

REQUIREMENTS
------------
No special requirements.

INSTALLATION
------------
Download and place the recommended version of the module in your website's
modules directory, go to the **Extend** page (`/admin/modules`) and enable the
**Protected Submissions** module.

Alternatively, just run on CLI:
```
composer require drupal/protected_submissions
drush -y en protected_submissions
```

CONFIGURATION
-------------
Go to the **Protected Submissions** configuration page
(`/admin/config/content/protected_submissions`) and set the reject message text
and the trigger patterns for rejection. Set needed permissions on 
`/admin/people/permissions#module-protected_submissions`.

TROUBLESHOOTING
---------------
Report all issues on
https://www.drupal.org/project/issues/search/protected_submissions.
