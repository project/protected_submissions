<?php

namespace Drupal\Tests\protected_submissions\Functional;

use Drupal\comment\Tests\CommentTestTrait;
use Drupal\Tests\BrowserTestBase;
use Drupal\node\Entity\NodeType;
use Drupal\Tests\node\Traits\NodeCreationTrait;
use Drupal\user\RoleInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * {@inheritdoc}
 */
class ProtectedSubmissionsTest extends BrowserTestBase {
  use NodeCreationTrait, CommentTestTrait;
  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';
  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'node',
    'comment',
    'protected_submissions',
  ];

  /**
   * {@inheritdoc}
   */
  public function testNodeCommentRejectedSubmissionPrintsCorrectMessage() {
    // Create an article content type (if it doesn't exist already).
    $types = NodeType::loadMultiple();
    if (empty($types['article'])) {
      $this->drupalCreateContentType(['type' => 'article', 'name' => $this->t('Article')]);
    }

    // Create comment field on article.
    $this->addDefaultCommentField('node', 'article');

    // Create an article node.
    $this->drupalCreateNode([
      'title' => $this->t('An Article'),
      'type' => 'article',
    ])->save();

    // Allow anonymous users to post comments.
    $permissions = [
      'access content',
      'access comments',
      'post comments',
      'skip comment approval',
    ];
    user_role_grant_permissions(RoleInterface::ANONYMOUS_ID, $permissions);

    // Add a custom string to the reject patterns list.
    $rejectPatterns = $this->config('protected_submissions.settings')->get('protected_submissions.reject_patterns');
    $inputString = ' monkey';

    $rejectPatterns .= ',' . $inputString;
    $this->config('protected_submissions.settings')
      ->set('protected_submissions.reject_patterns', $rejectPatterns)
      ->save();

    // Attempt to post a spam comment.
    $input = [
      'edit-comment-body-0-value' => $inputString,
    ];
    $this->drupalPostForm('node/1', $input, $this->t('Save'));

    // Ensure we get a 200 response.
    $this->assertResponse(Response::HTTP_OK);

    // Ensure the rejected message is displayed.
    $message = $this->config('protected_submissions.settings')->get('protected_submissions.reject_message');
    $this->assertSession()->responseContains($message);
    $this->assertSession()->responseNotContains('Your comment has been posted.');

    // Attempt to post a normal comment.
    $input = [
      'edit-comment-body-0-value' => 'a normal comment',
    ];
    $this->drupalPostForm('node/1', $input, $this->t('Save'));

    // Ensure we get a 200 response.
    $this->assertResponse(Response::HTTP_OK);

    // Ensure our normal post was posted.
    $this->assertSession()->responseContains('Your comment has been posted.');
    $this->assertSession()->responseContains('a normal comment');
  }

}
